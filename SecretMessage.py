def findMessage(input):	
	result = "".join(c for c in input if c.isupper())
	return result

assert findMessage("How are you? Eh, ok. Low or Lower? Ohhh.") == "HELLO"  #"hello"
assert findMessage("hello world!") == ""  #"Nothing"
assert findMessage("HELLO WORLD!!!") == "HELLOWORLD"  #"Capitals"