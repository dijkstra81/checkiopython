def evenTheLast(array):
	sum = 0
	i = 0
	if len(array)==0:
		return 0

	while (i<len(array)):
		if(i%2 == 0):
			sum+=array[i]
		i+=1		 
	result = sum*array[i-1]
	print(result)
	return result


assert evenTheLast([0, 1, 2, 3, 4, 5]) == 30 #"(0+2+4)*5=30"
assert evenTheLast([1, 3, 5]) == 30 #"(1+5)*5=30"
assert evenTheLast([6]) == 36 #"(6)*6=36"
assert evenTheLast([]) == 0 #"An empty array = 0"